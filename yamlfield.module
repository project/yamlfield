<?php

/**
 * Implements hook_field_info().
 */
function yamlfield_field_info() {
  return array(
    // We name our field as the associative name of the array.
    'yaml' => array(
      'label' => t('YAML'),
      'description' => t('Field for YAML code'),
      'default_widget' => 'yamlfield_widget',
      'default_formatter' => 'yamlfield_formatter',
      'property_type' => 'text',
      'instance_settings' => array(
        'validate' => FALSE,
        'format' => FALSE,
      ),
    ),
  );
}

/**
 * Implements hook_field_validate().
 */
function yamlfield_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  foreach ($items as $delta => $item) {
    if (!empty($item['yaml'])) {
      if ($instance['settings']['validate'] == 1 || $instance['settings']['format'] == 1) {
        if (function_exists('yaml_parse')) {
          $data = yaml_parse($item['yaml']);
          if (is_array($data)) {
            $item['yaml'] = yaml_emit($data);
          } else {
            $errors[$field['field_name']][$langcode][$delta][] = array(
              'error' => 'yamlfield_error',
              'message' => t('%name: is not valid YAML.', array('%name' => $instance['label'])),
            );
          }
        }
      }
    }
  }
}

/**
 * Implements hook_field_presave().
 */
function  yamlfield_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  foreach ($items as $delta => &$item) {
    if (!empty($item['yaml'])) {
      if ($instance['settings']['format'] == 1) {
        if (function_exists('yaml_parse')) {
          $data = yaml_parse($item['yaml']);
          if (is_array($data)) {
            $item['yaml'] = yaml_emit($data);
          }
        }
      }
    }
  }
}

/**
 * Implements hook_field_is_empty().
 */
function yamlfield_field_is_empty($item, $field) {
  return empty($item['yaml']);
}

/**
 * Implements hook_field_formatter_info().
 */
function yamlfield_field_formatter_info() {
  return array(
    // This formatter just displays the hex value in the color indicated.
    'yamlfield_plain' => array(
      'label' => t('Simple text-based formatter'),
      'field types' => array('yaml'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function yamlfield_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  switch ($display['type']) {
    // This formatter simply outputs the field as text and with a color.
    case 'yamlfield_plain':
      foreach ($items as $delta => $item) {
        $element[$delta] = array(
          '#type' => 'markup',
          '#suffix' => '</pre>',
          '#prefix' => '<pre>',
          '#markup' => $item['yaml'],
        );
      }
      break;
  }
  return $element;
}

/**
 * Implements hook_field_widget_info().
 */
function yamlfield_field_widget_info() {
  return array(
    'yamlfield_default' => array(
      'label' => t('YAML in plaintext'),
      'field types' => array('yaml'),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function yamlfield_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $value = isset($items[$delta]['yaml']) ? $items[$delta]['yaml'] : '';

  $widget = $element;
  $widget['#delta'] = $delta;

  $widget += array(
    '#title' => $instance['label'],
  );

  switch ($instance['widget']['type']) {
    case 'yamlfield_default':
      $widget += array(
        '#type' => 'textarea',
        '#default_value' => $value,
        '#size' => 7,
      );
      break;
  }
  
  $element['yaml'] = $widget;

  return $element;
}

/**
 * Implements hook_field_widget_error().
 */
function yamlfield_field_widget_error($element, $error, $form, &$form_state) {
  switch ($error['error']) {
    case 'yamlfield_error':
      form_error($element, $error['message']);
      break;
  }
}

/**
 * Implements hook_field_instance_settings_form()
 */
function yamlfield_field_instance_settings_form($field, $instance) {
  $settings = $instance['settings'];

  $form = array();

  if ($field['type'] == 'yaml') {

    $form['validate'] = array(
      '#type' => 'checkbox',
      '#disabled' => !function_exists('yaml_parse'),
      '#title' => t('Validate'),
      '#default_value' => isset($settings['validate']) ? $settings['validate'] : 0,
      '#description' => t('Require entered YAML code to be syntactically valid.'),
    );

    $form['format'] = array(
      '#type' => 'checkbox',
      '#disabled' => !function_exists('yaml_parse'),
      '#title' => t('Reformat'),
      '#default_value' => isset($settings['format']) ? $settings['format'] : 0,
      '#description' => t('Ensure proper YAML formatting. Note that this will process YAML anchors (&anchor, *anchor).'),
    );
  }

  return $form;
}
